axios.defaults.baseURL = 'http://www.liulongbin.top:3008'
// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  // 形参中的config是每次请求时候的配置选项，里面记录着这次请求对应的
  // method，url，根路径，headers请求头
  if (config.url.indexOf('/my') !== -1) {
    // 如果请求的url包含my，那么久带着Authorization = localStorage.getItem('token')
    config.headers.Authorization = localStorage.getItem('token')
  }

  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});