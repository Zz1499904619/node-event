$(function () {
  // 文章默认的发布状态
  let state = '已发布'

  // 点击草稿
  $('.btn_cg').on('click', function () {
    state = '草稿'
  })
  // 点击发布按钮
  $('.btn_pub').on('click', function () {
    state = '已发布'
  })
  layui.form.verify({
    // 正则中的点号表示匹配任意字符
    title: [/^.{1,30}$/, '标题必须是1-30位字符']
  })

  function initCatelist() {
    axios.get('/my/cate/list').then(({
      data: res
    }) => {
      if (res.code === 0) {
        console.log(res);
        const rows = []
        res.data.forEach(item => {
          rows.push(`<option value="${item.id}">${item.cate_name}</option>`)
        })
        $('[name="cate_id"]').append(rows)
        // 如果列表中某些元素是动态追加的，需要调用form.render
        // 从新渲染，否则内容无法显示
        // 告诉layui，文本框不需要select美化
        $('.ql-toolbar select').attr('lay-ignore', '')
        layui.form.render('select'),
          // render完毕之后，把quill的select全部隐藏
          $('.ql-toolbar select').hide()
        // console.log(res);
      }
    })
  }
  initCatelist()
  var toolbarOptions = [
    ['bold', 'italic', 'underline', 'strike', 'image'], // toggled buttons
    ['blockquote', 'code-block'],

    [{
      'header': 1
    }, {
      'header': 2
    }], // custom button values
    [{
      'list': 'ordered'
    }, {
      'list': 'bullet'
    }],
    [{
      'script': 'sub'
    }, {
      'script': 'super'
    }], // superscript/subscript
    [{
      'indent': '-1'
    }, {
      'indent': '+1'
    }], // outdent/indent
    [{
      'direction': 'rtl'
    }], // text direction

    [{
      'size': ['small', false, 'large', 'huge']
    }], // custom dropdown
    [{
      'header': [1, 2, 3, 4, 5, 6, false]
    }],

    [{
      'color': []
    }, {
      'background': []
    }], // dropdown with defaults from theme
    [{
      'font': []
    }],
    [{
      'align': []
    }],

    ['clean'] // remove formatting button
  ];
  var quill = new Quill('#editor', {
    modules: {
      toolbar: toolbarOptions
    },
    theme: 'snow'
  });
  // 模拟文件选择框的点击行为
  $('.btn-choose-img').on('click', function () {
    $('#file').click()
  })
  // 变量file用来存储用户选择的封面
  let file = null
  // 监听文件选择框的changge事件
  $('#file').on('change', function (e) {
    // 位数则,获得文件列表
    const files = e.target.files
    // 判断用户是否选择了文件,没有则return
    if (files.length === 0) {
      // 如果没有选择封面,则把file重置为null
      file = null
      return


    }
    file = files[0]
    const imgURL = URL.createObjectURL(files[0])
    $('#image').attr('src', imgURL)
  })

  $('.form-pub').on('submit', function (e) {
    e.preventDefault()
    // 1.先判断用户是否选择了封面
    // 1.1如果没有选择页面,提示用户必须选择,推出代码执行
    // 1.2如果选择了封面进行后续处理
    if (!file) {
      return layer.msg('请选择封面后再提交!')
    }
    // 1.准备formdata格式的请求体
    const fd = new FormData()
    fd.append('title', $('[name="title"]').val())
    fd.append('cate_id', $('[name="cate_id"]').val())
    fd.append('cover_img', file)
    fd.append('content', quill.root.innerHTML)
    fd.append('state', state)

    // 发送axios请求，调用发布文章接口
    axios.post('/my/article/add', fd).then(({
      data: res
    }) => {
      if (res.code === 0) {
        // 跳转到文章列表
        location.href = "/artcle/art_list.html"
        window.parent.highlight('文章列表')
      }
    })
  })
})