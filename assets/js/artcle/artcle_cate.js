$(function () {
  initCateList()

  function initCateList() {
    axios.get('/my/cate/list').then(({
      data: res
    }) => {
      const rows = []
      res.data.forEach((item, index) => {
        rows.push(` <tr>
        <td>${index+1}</td>
        <td>${item.cate_name}</td>
        <td>${item.cate_alias}</td>
        <td>
          <button type="button" class="layui-btn layui-btn-xs btn-edit"data-id=${item.id}>修改</button>
          <button type="button" class="layui-btn layui-btn-danger layui-btn-xs btn-delete"data-id=${item.id}>删除</button>
        </td>
      </tr>`)
      })
      $('tbody').html(rows)
    })
  }
  // 点击按钮,弹出层
  let addIndex = null
  $('#btnShowAdd').on('click', function () {
    addIndex = layer.open({
      type: 1,
      area: ['500px', '250px'],
      title: '添加文章分类',
      content: $('#template-add').html()
    });

  })
  // 自定义校验规则
  layui.form.verify({
    name: [/^\S{1,10}$/, '分类名称必须是1-10位的非空字符'],
    alias: [/^[a-zA-Z0-9]{1,15}$/, '分类别名必须是1-15位的字母和数字']
  })
  // 监听列表submit事件
  $('body').on('submit', '#form-add', function (e) {
    e.preventDefault()
    const data = $(this).serialize()
    axios.post('/my/cate/add', data).then(({
      data: res
    }) => {
      if (res.code === 0) {
        // 提示用户添加成功
        layer.msg('添加分类成功')
        // 刷新分类的列表数据
        initCateList()
        // 关闭弹出层
        layer.close(addIndex)
      }
    })
  })
  // 添加
  let editIndex = null
  $('tbody').on('click', '.btn-edit', function () {
    // console.log(00);
    const id = $(this).attr('data-id')
    if (id === '1' || id === '2') {
      return layer.msg('管理员不允许修改此数据!')
    }
    // 弹层
    editIndex = layer.open({
      type: 1,
      area: ['500px', '250px'],
      title: '修改文章分类',
      content: $('#template-edit').html()
    });
    // const id = $(this).attr('data-id')
    axios.get('/my/cate/info', {
      params: {
        id
      }
    }).then(({
      data: res
    }) => {
      if (res.code === 0) {
        layui.form.val('form-edit', res.data)
      }
    })

  })
  // 修改
  $('body').on('submit', '#form-edit', function (e) {
    e.preventDefault()
    const data = $(this).serialize()
    axios.put('/my/cate/info', data).then(({
      data: res
    }) => {
      if (res.code === 0) {
        // 提示用户添加成功
        layer.msg('修改分类成功')
        // 刷新分类的列表数据
        initCateList()
        // 关闭弹出层
        layer.close(editIndex)
      }
    })
  })
  // 删除
  $('tbody').on('click', '.btn-delete', function () {
    // console.log(00);
    const id = $(this).attr('data-id')
    if (id === '1' || id === '2') {
      return layer.msg('不允许删除此数据!')
    }
    layer.confirm('确认删除吗？', {
      icon: 3,
      title: '提示'
    }, function (index) {
      axios.delete('/my/cate/del', {
        params: {
          id
        }
      }).then(({
        data: res
      }) => {
        if (res.code === 0) {
          initCateList()
        }
        layer.close(index)
      })
    })
  })
})