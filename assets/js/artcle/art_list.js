$(function () {
  // 查询参数对象
  const q = {
    // 页码值
    pagenum: 1,
    // 每页展示数据条数
    pagesize: 2,
    // 筛选条件:文章分类
    cate_id: '',
    // 筛选条件:文章的发布状态,如果值为空,表示没有发布状态对应的筛选条件
    state: ''
  }
  // 获取文章列表数据
  function initArtList() {
    axios.get('/my/article/list', {
      params: q
    }).then(({
      data: res
    }) => {
      if (res.code === 0) {
        console.log(res);
        const rows = []
        res.data.forEach(item => {
          rows.push(`<tr>
          <td><a href="javascript:;" style="color:blue" class="show-detail" data-id=${item.id}>${item.title}</a></td>

          <td>${item.cate_name}</td>
          <td>${dayjs(item.pub_date).format('YYYY-MM-DD HH:mm:ss')}</td>
          <td>${item.state}</td>
          <td>
            <button type="button" class="layui-btn layui-btn-danger layui-btn-xs btn-delete" data-id=${item.id}>删除</button>
          </td>
        </tr>`)
        })
        $('tbody').html(rows)
        // 调用 renderPage(res.total),渲染分页效果
        renderPage(res.total)
      }
    })
  }
  initArtList()

  // 获取文章分类的函数
  function initCatelist() {
    axios.get('/my/cate/list').then(({
      data: res
    }) => {
      if (res.code === 0) {
        const rows = []
        res.data.forEach(item => {
          rows.push(`<option value="${item.id}">${item.cate_name}</option>`)
        })
        $('[name="cate_id"]').append(rows)
        layui.form.render('select')
      }
    })
  }
  initCatelist()
  // 筛选区域的表单绑定 submit事件
  $('form').on('submit', function (e) {
    e.preventDefault()
    // 根据用户指定的筛选条件,重新请求第一页的数据
    // 1.把用户勾选的分类id,存储到q.cata-id中
    // 2.把用户勾选的发布状态,存储到q.state中
    // 3. 把页码重置为1
    q.cate_id = $('[name="cate_id"]').val()
    q.state = $('[name="state"]').val()
    console.log(q.state);
    q.pagenum = 1
    initArtList()
  })
  $('[type="reset"]').on('click', function () {
    q.pagenum = 1
    q.cate_id = ''
    q.state = ''
    initArtList()
  })

  function renderPage(total) {
    layui.laypage.render({
      elem: 'page-box', //注意，这里的page-box是 ID，不用加 # 号
      count: total, //数据总数，从服务端得到
      limit: q.pagesize, //每页展示多少条数据
      curr: q.pagenum, //指定那个页码值需要高亮
      layout: ['count', 'limit', 'prev', 'page', 'next', 'skip'],
      limits: [1, 3, 5, 7, 9],

      jump: function (obj, first) {
        // obj包含了当前分页的多有参数
        console.log(obj.curr);
        // 得到当前页,向服务器请求对应页数据
        console.log(obj.limit); //得到每页显示的条数
        q.pagenum = obj.curr
        q.pagesize = obj.limit
        // 首次不执行
        if (!first) {
          initArtList()
        }
      }
    });
  }
  $('tbody').on('click', '.btn-delete', function () {
    const id = $(this).attr('data-id')
    console.log(id);
    layer.confirm('确认删除文章吗?', {
      icon: 3,
      title: '提示'
    }, function (index) {
      axios.delete('/my/article/info', {
        params: {
          id
        }
      }).then(({
        data: res
      }) => {
        if (res.code === 0) {
          layer.msg('删除数据成功!')
          if (q.pagenum > 1 && $('tbody tr').length === 0) {
            q.pagenum--
          }


          initArtList()
        }
      })
      layer.close(index)
    })

  })
  $('tbody').on('click', '.show-detail', function () {
    const id = $(this).attr('data-id')
    axios.get('/my/article/info', {
      params: {
        id
      }
    }).then(({
      data: res
    }) => {
      if (res.code === 0) {
        console.log(res);
        layer.open({
          type: 1,
          area: ["85%", "85%"],
          title: '预览文章',
          content: `<div class="artinfo-box">
          <h1 class="artinfo-title">${res.data.title}</h1>
          <div class="artinfo-bar">
            <span>作者：${res.data.nickname||res.data.username}</span>
            <span>发布时间：${dayjs(res.data.pub_date).format('YYYY-MM-DD HH:mm:ss')}</span>
            <span>所属分类：${res.data.cate_name}</span>
            <span>状态：${res.data.state}</span>
          </div>
          <hr>
          <img src="http://www.liulongbin.top:3008${res.data.cover_img}" alt="" class="artinfo-cover">
          <div>文章的内容</div>
          </div>`
        })
      }
    })
  })
})