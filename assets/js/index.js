$(function () {
  $('.logout').on('click', function () {
    console.log('ok');
    // 询问用户是否退出
    // 如果用户确认退出
    // 清除token
    // 跳转到登录页面
    layer.confirm('确认退出登陆吗？', {
      icon: 3,
      title: '提示'
    }, function (index) {
      localStorage.removeItem('token'),
        location.href = '/login.html'
      // 关闭弹出层
      layer.close(index)
    })
  })


  initUSerInfo()



})

function initUSerInfo() {
  // 产生401相应两种情况
  // 没有把token发给服务器
  // 发送假的token
  // Promise.prototype.then(成功的回调，失败的回调)
  axios.get('/my/userinfo',
    // {
    //   // 自定义请求体
    //   headers: {
    //     Authorization: localStorage.getItem('token')
    //   }
    // }
  ).then(({
    data: res
  }) => {
    console.log(res);
    console.log('成功了');
    // 调用渲染用户的函数
    renderUserInfo(res.data)
  }, (error) => {
    console.log('失败了');
    console.log(error);
    // 判断响应状态是否是401
    // 如果是，清空token，跳转到登录页
    if (error.response.status === 401) {
      localStorage.removeItem('token')
      location.href = '/login.html'
    }
  })
}

function renderUserInfo(data) {
  const name = data.nickname || data.username
  const textAvatar = name.charAt(0).toUpperCase()
  // 渲染头部区域的用户信息
  // 判断用户是否有图片的头像,有执行if,没有else
  if (data.user_pic) {
    //  有图片头像
    $('#header-avatar').html(` <img src="${data.user_pic}"
    class="layui-nav-img">  个人中心`)
  } else {

    // 没有图片的头像,渲染成文本信息
    $('#header-avatar').html(` <div class="text-avatar">${textAvatar}</div>  个人中心`)
  }
  // 渲染侧边区域的用户信息
  if (data.user_pic) {
    $('.user-info-box').html(`<img src="${data.user_pic}"
    class="layui-nav-img">
  <span class="welcome">&nbsp;欢迎&nbsp; ${name}</span>`)
  } else {
    $('.user-info-box').html(`<div class="text-avatar">${textAvatar}</div>
        <span class="welcome">&nbsp;欢迎&nbsp; ${name} </span>`)
  }
  // 在页面元素动态生成好之后,调用layui提供的element.render函数
  // 重新渲染指定区域的效果
  layui.element.render('nav', 'header-nav')



}

function highlight(kw) {
  console.log(666);
  $('dd').removeClass('layui-this')
  $(`dd:contains("${kw}")`).addClass('layui-this')
}