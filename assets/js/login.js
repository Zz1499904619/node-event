$(function () {
  $("#link-reg").on('click', function () {
    // 展示注册
    $('.reg-box').show()
    // 隐藏登录
    $(".login-box").hide()
  })


  // 点击去登陆的链接
  $('#link-login').on('click', function () {
    // 展示登录
    $('.login-box').show()
    // 隐藏注册
    $('.reg-box').hide()
  })

  // 自定义校验规则
  layui.form.verify({
    // 键值对格式  键:值
    // 自定义校验规则的名字：自定义的校验规则
    //uname：[正则，错误提示]
    uname: [/^[a-zA-Z0-9]{1,10}$/, '用户名必须是1-10位的字母和数字'],
    pwd: [/^\S{6,15}$/, '密码必须是6-15位非空字符'],
    // 判断两次密码是否一致
    repwd: function (value) {
      // 形参中的value是确认密码框的值
      // 应该和密码框得值对比
      const pwd = $('.reg-box [name = "password"]').val()
      if (value !== pwd) {
        return '密码不一致'
      }
    }
  })
  // 监听表单的submit
  $('.reg-box form').on('submit', function (e) {
    // 阻止表单的默认提交行为
    e.preventDefault()
    // 发起post请求
    // 判断是否请求成功
    // 提示用户注册成功
    // 隐藏注册，站式登录
    axios.post('http://www.liulongbin.top:3008/api/reg', $(this).serialize()).then(({
      data: res
    }) => {
      if (res.code === 0) {
        // 成功
        layer.msg('注册成功')
        $('#link-login').click()
      } else {
        // 失败
        layer.msg(res.message)
      }
    })
  })
  // 监听登录表单的submit事件
  $('.login-box form').on('submit', function (e) {
    e.preventDefault()
    axios.post('http://www.liulongbin.top:3008/api/login', $(this).serialize()).then(({
      data: res
    }) => {
      if (res.code === 0) {
        // 登录成功
        localStorage.setItem('token', res.token)
        // 把token的值储存到本地
        layer.msg('登录成功')
        // 跳转到后台主页
        location.href = '/index.html'
      } else {
        // 登录失败
        layer.msg('登陆失败')
        // 移除token
        localStorage.removeItem('token')
      }
    })
  })
})