$(function () {
  // 点击了选择图片按钮
  $('#btnChooseImg').on('click', function () {
    // 模拟点击事件
    $('#file').click()
  })
  let file = null;

  $('#file').on('change', function (e) {
    // 1.获取到用户选择的文件列表
    const files = e.target.files
    // 2.判断用户是否选择了文件
    if (files.length === 0) {
      // 如果用户没有选择文件,则把file重置为null
      file = null
      return
    }
    // 为全局的file文件赋值
    file = files[0]
    // 3. 如果用户选择了文件, 则把图片渲染到页面的i每个标签中/
    // 3.1 URL.createObjectURL()函数接收一个文件,返回值是这个文件的URL地址
    const imgURL = URL.createObjectURL(files[0])
    $('#image').attr('src', imgURL)
  })
  $('#btnUploadImg').on('click', function () {
    if (!file) {
      layer.msg('请先上传头像')
      return
    }
    // 1.用户选择的图片文件,转为base64格式
    // 2.调用axios发起请求,上传头像
    // 3.如果头像上传成功,应该更新父页面(index)中用户的头像
    const fr = new FileReader()
    // fr.readAsDataURL(要读取的文件)
    fr.readAsDataURL(file)
    // 监听事件 读取完成的load事件
    fr.addEventListener('load', function () {
      // 获取读取的结果
      console.log(fr.result);
      // 请求上传图片
      // 如果上传成功,提示用户
      // 需要更新父元素中 用户的基本信息
      axios.patch('/my/update/avatar', {
        avatar: fr.result
      }).then(({
        data: res
      }) => {
        if (res.code === 0) {
          layer.msg('更新头像成功', {
            icon: 1
          })
          window.parent.initUSerInfo()
        }
      })
    })
  })
})