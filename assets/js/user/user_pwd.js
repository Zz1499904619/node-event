$(function () {

  // 自定义校验
  layui.form.verify({
    pwd: [/^\S{6,15}$/, '密码必须是6-15位非空字符'],
    newpwd: function (value) {
      const newpwd = $('.layui-form [name = "old_pwd"]').val()
      if (value === newpwd) {
        return '两次密码一致'
      }
    },
    repwd: function (value) {
      const newpwd = $('.layui-form [name = "new_pwd"]').val()
      if (value !== newpwd) {
        return '密码不一致'
      }
    }

  })
  $('.layui-form').on('submit', function (e) {
    e.preventDefault()
    axios.patch('/my/updatepwd', $(this).serialize()).then(({
      data: res
    }) => {
      if (res.code === 1) {
        layer.msg('更新密码失败')
      } else {
        layer.msg(res.message)
      }
    })
  })
})