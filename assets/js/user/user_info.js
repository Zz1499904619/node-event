$(function () {
  function initUserInfo() {
    // axios发起请求，获取请求用户信息
    axios.get('/my/userinfo', ).then(({
      data: res
    }) => {
      console.log(res);
      layui.form.val('user-form', res.data)
    })
  }
  initUserInfo()

  // 自定义校验规则
  layui.form.verify({
    nickname: [/^\S{1,10}$/, '昵称必须是1-10位的非空字符']
  })
  $('[lay-filter="user-form"]').on('submit', function (e) {
    e.preventDefault()
    const data = $(this).serialize()
    console.log(data);
    axios.put('/my/userinfo', data).then(({
      data: res
    }) => {
      if (res.code === 0) {
        layer.msg('更新用户资料成功')
        window.parent.initUSerInfo()
      }
    })
  })
  $('[type="reset"]').on('click', function (e) {
    // 阻止重置按钮的默认行为
    e.preventDefault()
    // 重新请求用户的信息，并填充到表单中即可
    initUserInfo()
  })
})